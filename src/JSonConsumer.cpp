#include "JSonConsumer.hpp"
#include "ParseError.hpp"

using std::string;
using std::pair;
using std::list;

JSonConsumer::JSonConsumer()
{ }

JSonConsumer::~JSonConsumer()
{
	deleteChild();
}

void JSonConsumer::deleteChild()
{
	for (list<pair<string, JSonConsumer*> >::iterator i = child.begin(); i != child.end(); i++)
		delete (*i).second;
	child.clear();
}

template <>
const list<pair<string, JSonConsumer *> > &JSonConsumer::get() const
{ return child; }

template<>
const string &JSonConsumer::get() const
{ return value; }

void JSonConsumer::clear()
{
	value = string();
	deleteChild();
}

list<string> *JSonConsumer::strToList(const string &str)
{
	const char *data = str.data();
	const size_t _size = str.size();
	list<string> *result = new list<string>();
	bool inString =false;
	bool escaped  =false;
	bool inWord =false;
	size_t begin =0;

	for (size_t i=0; i < _size; i++)
	{
		if (data[i] == '"')
		{
			if (inString && !escaped)
			{
				result->push_back(string(&data[begin], i -begin +1));
				begin = i +1;
				inString = inWord = false;
			}
			else if (!inString)
			{
				inString = inWord = true;
				begin = i;
			}
			else if (escaped)
				escaped = false;
			else
				throw ParseError("Mismatched '\"'");
		}
		else if (data[i] == '\\')
			escaped = !escaped;
		else if (((data[i] >= '0' && data[i] <= '9') ||
				(data[i] >= 'a' && data[i] <= 'z') ||
				(data[i] >= 'A' && data[i] <= 'Z')) && !inString)
		{
			if (!inWord)
			{
				begin = i;
				inWord = true;
			}
		}
		else if (data[i] == ' ' || data[i] == '\t' || data[i] == '\n' || data[i] == '\r')
			;
		else if (!inString)
		{
			if (inWord)
			{
				string s (&data[begin], i-begin);
				while (s.size() && (*(s.end() -1)) == ' ')
					s.erase(s.end() -1);
				result->push_back(s);
				result->push_back(string(&data[i], 1));
				inString = inWord = false;
			}
			else
				result->push_back(string(&data[i], 1));
		}

	}
	return result;
}

void JSonConsumer::consume(const list<string> *words, list<string>::iterator &i)
{
	bool isObj = false;
	string name;

	for (; i != words->end();)
	{
		if ((*i == "[" || *i == ",") && !isObj)
		{
			JSonConsumer *child = new JSonConsumer();

			i++;
			child->consume(words, i);
			this->child.push_back(std::make_pair("", child));
		}
		else if (*i == "{" || (*i == "," && isObj))
		{
			JSonConsumer *child = new JSonConsumer();
			isObj = true;

			i++; //name
			child->consume(words, i);
			this->child.push_back(std::make_pair("", child));
			i++; //value
			child = new JSonConsumer();
			child->consume(words, i);
			this->child.push_back(std::make_pair("", child));
		}
		else if (*i == "}" || *i == "]")
		{
			i++;
			return;
		}
		else
		{
			value = *i;
			i++;
			return;
		}
	}
}

void JSonConsumer::consume(const string &str)
{
	list<string> *words;
	words = JSonConsumer::strToList(str);
	list<string>::iterator it = words->begin();

	this->consume(words, it);
	delete words;
}

#include <iostream>

void JSonConsumer::debug(unsigned int size)
{
	if (value != "")
	{
		for (unsigned int i =0; i < size; i++)
			std::cout << " ";
		std::cout << value << std::endl;
	}
	else
		for (list<pair<string, JSonConsumer *> >::iterator i =child.begin(); i != child.end(); i++)
		{
			(*i).second->debug(size +1);
			std::cout << "----" << std::endl;
		}
}

