#pragma once
#include "ParseError.hpp"

template <class T>
std::string JSonSerialize<T>::toString() const
{
	return JSonPrivateObject::toString(*this);
}

template<>
std::string JSonSerialize<std::string>::_toString() const
{
	std::string value = **this;
	size_t pos =0;

	while (true)
	{
		pos = value.find_first_of("\"\\", pos);
		if (pos == value.npos)
			break;
		value.insert(pos, "\\");
		pos += 2;
	}
	return "\"" +value +"\"";
}

template<>
void JSonSerialize<std::string>::fromString(const std::string &v)
{
	size_t pos;
	bool inString = false;
	value = v;

	if ((pos = v.size()) == 0)
		return;
	pos--;
	if (value[pos] == '\"' && pos && value[pos -1] != '\\')
	{
		inString = true;
		value.erase(pos, 1);
		pos--;
	}
	while (pos > 0)
	{
		if (value[pos] == '\\')
		{
			value.erase(pos, 1);
			pos++;
		}
		pos--;
	}
	if (inString && value[0] == '\"')
		value.erase(0, 1);
	else if (inString)
		throw std::runtime_error("parse error: expected \"");
}

template<>
void JSonSerialize<std::string>::fromString(const IConsumer *v)
{
	return this->fromString(((JSonConsumer *)v)->get<std::string>());
}

template<> std::string JSonSerialize<bool>::_toString() const
{
	return value ? "true" : "false";
}

template<>
void JSonSerialize<bool>::fromString(const std::string &v)
{
	value = !(v == "false" || v == "0" || v == "");
}

template<>
void JSonSerialize<bool>::fromString(const IConsumer *v)
{
	return this->fromString(((JSonConsumer *)v)->get<std::string>());
}

template <class T>
void JSonSerialize<JSonSerialize<T> >::fromString(const std::string &s)
{
	JSonSerialize<T> item;
	(item).fromString(s);
	*this = *item;
}

template <class T>
void JSonSerialize<JSonSerialize<T> >::fromString(const IConsumer *v)
{
	JSonSerialize<T> item;
	(item).fromString(v);
	*this = *item;
}

template <class T>
std::string JSonSerialize<JSonSerialize<T> >::_toString() const
{
	JSonSerialize<T> v(**this);
	return v.toString();
}

template<class T> std::string JSonSerialize<T>::_toString() const
{
	std::stringstream ss;
	ss << **this;
	return ss.str();
}

template<class T>
void JSonSerialize<T>::fromString(const std::string &v)
{
	std::stringstream ss(v);
	ss >> **this;
}

template<class T>
void JSonSerialize<T>::fromString(const IConsumer *v)
{
	return this->fromString(((JSonConsumer *)v)->get<std::string>());
}

template<class T>
void JSonSerialize<std::list<T> >::fromString(const IConsumer *jsonConsumer)
{
	JSonConsumer::jsonChild list;

	this->value.clear();
	list = ((JSonConsumer*)jsonConsumer)->get<JSonConsumer::jsonChild>();
	for (JSonConsumer::jsonChild_iterator i = list.begin(); i != list.end(); i++)
	{
		JSonSerialize<T> subitem;
		subitem.fromString((*i).second);
		this->value.push_back(*subitem);
	}
}

template<class T>
void JSonSerialize<std::list<T> >::fromString(const std::string &s)
{
	JSonConsumer jsonConsumer;
	JSonConsumer::jsonChild list;

	jsonConsumer.consume(s);
	return this->fromString(&jsonConsumer);
}

template <class T>
std::string JSonSerialize<std::list<T> >::toString() const
{
	return JSonPrivateObject::toString(*this);
}

template <class T>
std::string JSonSerialize<JSonSerialize<T> >::toString() const
{
	return JSonPrivateObject::toString(*this);
}

template <class U>
std::string JSonSerialize<std::list<U> >::_toString() const
{
	std::string result = "[";
	bool first = true;

	for (typename std::list<U>::const_iterator i = (**this).begin(); i != (**this).end(); i++)
	{
		U item_value = *i;
		JSonSerialize<U> item(item_value);
		if (!first)
			result += ",";
		result += item.toString();
		first = false;
	}
	return result +"]";
}

std::string JSonPrivateObject::toString(const ISerializable&) const
{
	return _toString();
}

void JSonSerialize<void>::fromString(const IConsumer *jsonConsumer)
{
	JSonConsumer::jsonChild list;

	list = ((JSonConsumer*)jsonConsumer)->get<JSonConsumer::jsonChild>();
	if (list.size() == 1)
		return this->fromString((*(list.begin())).second);
	for (JSonConsumer::jsonChild_iterator i = list.begin(); i != list.end(); i++)
	{
		JSonSerialize<std::string> subitem;
		subitem.fromString((*i).second);
		if (++i == list.end())
			throw ParseError("Expected ':', got END");
		ISerializable *s = this->objects.at(*subitem);
		s->fromString((*i).second);
	}
}

void JSonSerialize<void>::fromString(const std::string &s)
{
	JSonConsumer jsonConsumer;
	JSonConsumer::jsonChild list;

	jsonConsumer.consume(s);
	return this->fromString(&jsonConsumer);
}

std::string JSonSerialize<void>::toString() const
{
	return JSonPrivateObject::toString(*this);
}

std::string JSonSerialize<void>::_toString() const
{
	std::string result = "{";
	bool first = true;

	for (std::map<const std::string, ISerializable *>::const_iterator i =objects.begin();
			i != objects.end();
			i++)
	{
		if (!first)
			result += ",";
		result += (*i).first +":";
		result += (*i).second->toString();
		first = false;
	}
	result += "}";
	return result;
}

