#pragma once

#include <list>
#include <stdexcept>
#include <sstream>

#include "JSonConsumer.hpp"
#include "ASerializable.hpp"

#define JSON_CONSTRUCT(T, TF) \
	JSonSerialize(): ASerializable<TF>() {};\
	JSonSerialize(const std::string &n, ASerializable<void> &parent): ASerializable<TF>(n, parent) {};\
	JSonSerialize(const std::string &n, ASerializable<void> &parent, const T &value): ASerializable<TF>(n, parent, value) {};\
	JSonSerialize(const T &value): ASerializable<TF>(value) {};
#define CEREALIZER_JSON_FROMSTRING_TOSTRING \
	void fromString(const IConsumer *);\
	void fromString(const std::string &);\
	virtual std::string toString() const;\

class JSonPrivateObject
{
	public:
		std::string toString(const ISerializable &) const;

	protected:
		virtual std::string _toString() const =0;
};

template <class T>
class JSonSerialize: public JSonPrivateObject, public ASerializable<T>
{
public:
	JSON_CONSTRUCT(T, T)
	CEREALIZER_JSON_FROMSTRING_TOSTRING

protected:
	std::string _toString() const;
};

template <class T>
class JSonSerialize<JSonSerialize<T> >: public JSonPrivateObject, public ASerializable<T>
{
public:
	JSON_CONSTRUCT(T, T)
	JSonSerialize(const ASerializable<T> &value): ASerializable<T>(*value) {};
	CEREALIZER_JSON_FROMSTRING_TOSTRING

protected:
	std::string _toString() const;
};

template <>
class JSonSerialize<void>: public JSonPrivateObject, public ASerializable<void>
{
public:
	JSonSerialize(): ASerializable<void>() {};
	JSonSerialize(const std::string &n, ASerializable<void> &parent): ASerializable<void>(n, parent) {};
	CEREALIZER_JSON_FROMSTRING_TOSTRING

protected:
	virtual std::string _toString() const;
};

template <class T>
class JSonSerialize<std::list<T> >: public JSonPrivateObject, public ASerializable<std::list <T> >
{
public:
	JSON_CONSTRUCT(T, std::list<T>)
	JSonSerialize(const std::list<T> &a): ASerializable<std::list<T> >(a) {};
	CEREALIZER_JSON_FROMSTRING_TOSTRING

protected:
	std::string _toString() const;
};

#include "JSonSerialize_content.hpp"
#undef JSON_CONSTRUCT
#undef CEREALIZER_JSON_FROMSTRING_TOSTRING

