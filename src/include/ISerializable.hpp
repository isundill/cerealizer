#pragma once

#include <string>

class IConsumer
{};

class ISerializable
{
	public:
		virtual std::string toString() const =0;
		//virtual void toFile(const std::string &file)const =0;
		virtual void fromString(const std::string &string) =0;
		virtual void fromString(const IConsumer *) =0;
		//virtual void fromFile(const std::string &path) =0;
};

