#pragma once

#include <map>
#include "ISerializable.hpp"

template <class T>
class ASerializable: public ISerializable
{
public:
	ASerializable();																//call child's default constructor
	ASerializable(const std::string &name, ASerializable<void> &parent); 					//register parent's child
	ASerializable(const std::string &name, ASerializable<void> &parent, const T &value);	//register parent's child and set value
	ASerializable(const T &value);													//set value

	virtual T &operator*();
	virtual const T &operator*() const;

protected:
	T value;
};

template <>
class ASerializable<void>: public ISerializable
{
public:
	ASerializable() {};																//call child's default constructor
	ASerializable(const std::string &name, ASerializable<void> &parent); 					//register parent's child
	ASerializable(const std::string &name, ASerializable<void> &parent, const std::map<const std::string, ISerializable *> &value);	//register parent's child and set value
	ASerializable(const std::map<const std::string, ISerializable *> &value);													//set value

	virtual ASerializable<void> &registerParameter(const std::string &name, ISerializable &object);
	virtual const std::map<const std::string, ISerializable *> getChild() const;

protected:
	std::map<const std::string, ISerializable *> objects;
};

template <class T>
class ASerializable<ASerializable<T> >: public ISerializable
{
public:
	ASerializable();										//call child's default constructor
	ASerializable(const ASerializable<T> &value);			//set value
	ASerializable(const T &value);							//set value

	virtual T &operator*();
	virtual const T &operator*() const;

protected:
	T value;
};

template <class T>
ASerializable<T>::ASerializable()
{ }

template <class T>
ASerializable<T>::ASerializable(const T &value)
{
	this->value = value;
}

template <class T>
ASerializable<T>::ASerializable(const std::string &name, ASerializable<void> &parent)
{
	parent.registerParameter(name, *this);
}

template <class T>
ASerializable<T>::ASerializable(const std::string &name, ASerializable<void> &parent, const T &v)
{
	value = v;
	parent.registerParameter(name, *this);
}

ASerializable<void> &ASerializable<void>::registerParameter(const std::string &name, ISerializable &object)
{
	objects[name] = &object;
	return *this;
}

const std::map<const std::string, ISerializable *>ASerializable<void>::getChild() const
{ return objects; }

template <class T>
T &ASerializable<T>::operator *()
{ return (T&)value; }

template <class T>
const T &ASerializable<T>::operator *() const
{ return (T&)value; }

