#pragma once

#include <utility>
#include <string>
#include <list>

#include "ISerializable.hpp"

class JSonConsumer: public IConsumer
{
	public:
		typedef std::list<std::pair<std::string, JSonConsumer *> > jsonChild;
		typedef std::list<std::pair<std::string, JSonConsumer *> >::iterator jsonChild_iterator;

	public:
		JSonConsumer();
		~JSonConsumer();

		template <class T> const T& get() const;
		void consume(const std::string &str);
		static std::list<std::string> *strToList(const std::string &s);

	private:
		void consume(const std::list<std::string> *, std::list<std::string>::iterator &);
		void clear();
		void deleteChild();

		std::string value;
		std::list<std::pair<std::string, JSonConsumer *> > child;

	protected:
		void debug(unsigned int size =0);
};

