#include "ParseError.hpp"

ParseError::ParseError(const std::string &msg): std::runtime_error(msg)
{ }

