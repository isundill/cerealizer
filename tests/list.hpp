#pragma once

SUITE(List_List_Values)
{
	TEST(From_ListList__To_Listlist)
	{
		JSonSerialize<std::list<std::list<bool> > >json;

		json.fromString("[[true, true, true], [false, true, true], [false, false, false]]");
		std::list<bool> a, b, c;
		std::list<std::list<bool> >::iterator i = (*json).begin();
		a = *i;
		i++;
		b = *i;
		i++;
		c = *i;
		std::list<bool>::iterator j = a.begin();
		CHECK_EQUAL(*j, true);
		j++;
		CHECK_EQUAL(*j, true);
		j++;
		CHECK_EQUAL(*j, true);
		j = b.begin();
		CHECK_EQUAL(*j, false);
		j++;
		CHECK_EQUAL(*j, true);
		j++;
		CHECK_EQUAL(*j, true);
		j = c.begin();
		CHECK_EQUAL(*j, false);
		j++;
		CHECK_EQUAL(*j, false);
		j++;
		CHECK_EQUAL(*j, false);

		std::string ts = json.toString();
		CHECK_EQUAL("[[true,true,true],[false,true,true],[false,false,false]]", ts);
	}
}

SUITE(List_values)
{
	TEST(To_String_From_Stringlist)
	{
		test_stringList json;
		(*json).push_back("1");
		(*json).push_back("2");
		(*json).push_back("3");
		(*json).push_back("4");
		CHECK_EQUAL("[\"1\",\"2\",\"3\",\"4\"]", json.toString());
	}

	TEST(To_String_From_intList)
	{
		test_intList json2;
		(*json2).push_back(1);
		(*json2).push_back(2);
		(*json2).push_back(3);
		(*json2).push_back(4);
		CHECK_EQUAL("[1,2,3,4]", json2.toString());
	}

	TEST(To_String_From_BoolList)
	{
		test_boolList json2;
		(*json2).push_back(true);
		(*json2).push_back(false);
		(*json2).push_back(false);
		(*json2).push_back(false);
		CHECK_EQUAL("[true,false,false,false]", json2.toString());
	}

	TEST(To_String_From_JSonBoolList)
	{
		test_boolList json2;
		(*json2).push_back(true);
		(*json2).push_back(false);
		(*json2).push_back(false);
		(*json2).push_back(false);
		CHECK_EQUAL("[true,false,false,false]", json2.toString());
	}

	TEST(From_JsonBoolList)
	{
		JSonSerialize<std::list<JSonSerialize<bool> > > j;
		j.fromString("[true,false]");
		CHECK_EQUAL(2, (*j).size());
		std::list<JSonSerialize<bool> >::const_iterator i = (*j).begin();
		const JSonSerialize<bool> a = *i;
		i++;
		const JSonSerialize<bool> b = *i;
		CHECK_EQUAL(true, *a);
		CHECK_EQUAL(false, *b);
	}

	TEST(From_BoolList)
	{
		test_boolList j;
		j.fromString("[true,false]");
		CHECK_EQUAL(2, (*j).size());
		bool a, b;
		std::list<bool>::const_iterator i = (*j).begin();
		a = *i;
		i++;
		b = *i;
		CHECK_EQUAL(true, a);
		CHECK_EQUAL(false, b);

		j.fromString("  [ true ,   false ]");
		CHECK_EQUAL(2, (*j).size());
		i = (*j).begin();
		a = *i;
		i++;
		b = *i;
		CHECK_EQUAL(true, a);
		CHECK_EQUAL(false, b);
	}

	TEST(From_intList)
	{
		test_intList j;
		j.fromString("[1, 2, 3]");
		CHECK_EQUAL(3, (*j).size());
		int a, b, c;
		std::list<int>::const_iterator i = (*j).begin();
		a = *i;
		i++;
		b = *i;
		i++;
		c = *i;
		CHECK_EQUAL(1, a);
		CHECK_EQUAL(2, b);
		CHECK_EQUAL(3, c);

		(*j).clear();
		(*j).push_back(42);
		(*j).push_front(3);
		(*j).push_front(12);
		// 12 3 42
		std::string s = j.toString();
		CHECK_EQUAL("[12,3,42]", s);
		test_intList j2;
		j2.fromString(s);
		i = (*j).begin();
		a = *i;
		i++;
		b = *i;
		i++;
		c = *i;
		CHECK_EQUAL(12, a);
		CHECK_EQUAL(3, b);
		CHECK_EQUAL(42, c);
	}

	TEST(From_stringList)
	{
		test_stringList j;
		j.fromString("[1, \"2\", \"Maison\", \"Cou\\\"cou\"]");
		CHECK_EQUAL(4, (*j).size());
		std::string a, b, c, d;
		std::list<std::string>::const_iterator i = (*j).begin();
		a = *i;
		i++;
		b = *i;
		i++;
		c = *i;
		i++;
		d = *i;
		CHECK_EQUAL("1", a);
		CHECK_EQUAL("2", b);
		CHECK_EQUAL("Maison", c);
		CHECK_EQUAL("Cou\"cou", d);

	}
}

