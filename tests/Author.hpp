#pragma once

typedef std::string string;

class Book: public JSonSerialize<void>
{
	JSonSerialize<short>    editYear;
	JSonSerialize<string>   title;
};

typedef std::list<Book> BookList;

class Author: public JSonSerialize<void>
{
	JSonSerialize<short>    birthYear;
	JSonSerialize<short>    deathYear;
	JSonSerialize<string>   foreName;
	JSonSerialize<string>   lastName;
	BookList                books;
};

