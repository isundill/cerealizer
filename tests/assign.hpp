#pragma once

SUITE(Assign)
{
	TEST(Assign)
	{
		test_bool test;

		*test = true;
		CHECK_EQUAL(true, *test);
		*test = false;
		CHECK_EQUAL(false, *test);
	}

	TEST(Assign_list)
	{
		test_intList test;
		std::list<int> tmp;
		std::list<bool> tmp2;

		*test = std::list<int>();
		CHECK_EQUAL(0, (*test).size());
		(*test).push_back(42);
		CHECK_EQUAL(1, (*test).size());

		tmp.push_back(12);
		tmp.push_back(12);
		*test = tmp;
		CHECK_EQUAL(2, (*test).size());
	}
}

