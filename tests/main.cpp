#include <iostream>
#include <UnitTest++.h>
#include "JSonSerialize.hpp"

typedef JSonSerialize<bool> 					test_bool;
typedef JSonSerialize<int> 						test_int;
typedef JSonSerialize<float> 					test_float;
typedef JSonSerialize<std::string>				test_string;
typedef JSonSerialize<std::list<int> >			test_intList;
typedef JSonSerialize<std::list<std::string> >	test_stringList;
typedef JSonSerialize<std::list<bool> >			test_boolList;

#include "assign.hpp"
#include "values.hpp"
#include "list.hpp"
#include "objects.hpp"

int main()
{
	return UnitTest::RunAllTests();
}

