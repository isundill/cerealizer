#pragma once

SUITE(Boolean_values)
{
	TEST(From_String)
	{
		test_bool json;

		json.fromString("false");
		CHECK_EQUAL(false, *json);
		json.fromString("true");
		CHECK_EQUAL(true, *json);
		json.fromString("");
		CHECK_EQUAL(false, *json);
		json.fromString("0");
		CHECK_EQUAL(false, *json);
		json.fromString("1");
		CHECK_EQUAL(true, *json);
		json.fromString("42");
		CHECK_EQUAL(true, *json);
	}

	TEST(To_String)
	{
		test_bool json;

		json.fromString("1");
		CHECK_EQUAL("true", json.toString());
		json.fromString("0");
		CHECK_EQUAL("false", json.toString());
	}
}

SUITE(String_values)
{
	TEST(From_String)
	{
		test_string json;
		
		json.fromString("Test");
		CHECK_EQUAL("Test", *json);
		json.fromString("\"Test\"");
		CHECK_EQUAL("Test", *json);
		json.fromString("\"Te\\\"st\"");
		CHECK_EQUAL("Te\"st", *json);
		json.fromString("");
		CHECK_EQUAL("", *json);
		json.fromString("\"\"");
		CHECK_EQUAL("", *json);
	}

	TEST(To_String)
	{
		test_string json;

		*json = "Test";
		CHECK_EQUAL("\"Test\"", json.toString());
		*json = "\"Test\"";
		CHECK_EQUAL("\"\\\"Test\\\"\"", json.toString());
	}
}

SUITE(Floating_values)
{
	TEST(From_String)
	{
		test_float json;
		
		json.fromString("42");
		CHECK_EQUAL(42, *json);
		json.fromString("0");
		CHECK_EQUAL(0, *json);
		json.fromString("-12");
		CHECK_EQUAL(-12, *json);
		json.fromString("42.0");
		CHECK_EQUAL(42.f, *json);
		json.fromString("0.0");
		CHECK_EQUAL(0.f, *json);
		json.fromString("-12.2");
		CHECK_EQUAL(-12.2f, *json);
		json.fromString("1.");
		CHECK_EQUAL(1.f, *json);
		json.fromString(".03");
		CHECK_EQUAL(.03f, *json);
	}

	TEST(To_String)
	{
		test_float json;
		std::string tmp;

		*json = 42;
		CHECK_EQUAL("42", json.toString());
		*json = 0;
		CHECK_EQUAL("0", json.toString());
		*json = -12;
		CHECK_EQUAL("-12", json.toString());
		*json = 42.0;
		tmp = json.toString();
		json.fromString(tmp);
		CHECK_EQUAL(42.f, *json);
		*json = 0.0;
		tmp = json.toString();
		json.fromString(tmp);
		CHECK_EQUAL(0.f, *json);
		*json = -12.2;
		tmp = json.toString();
		json.fromString(tmp);
		CHECK_EQUAL(-12.2f, *json);
		*json = 1.;
		tmp = json.toString();
		json.fromString(tmp);
		CHECK_EQUAL(1.f, *json);
		*json = .03;
		tmp = json.toString();
		json.fromString(tmp);
		CHECK_EQUAL(.03f, *json);
	}
}

SUITE(Numeric_values)
{
	TEST(From_String)
	{
		test_int json;
		
		json.fromString("42");
		CHECK_EQUAL(42, *json);
		json.fromString("0");
		CHECK_EQUAL(0, *json);
		json.fromString("-12");
		CHECK_EQUAL(-12, *json);
	}

	TEST(To_String)
	{
		test_int json;

		*json = 42;
		CHECK_EQUAL("42", json.toString());
		*json = 0;
		CHECK_EQUAL("0", json.toString());
		*json = -12;
		CHECK_EQUAL("-12", json.toString());
	}
}

