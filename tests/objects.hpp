#pragma once

SUITE(Objects)
{
	class TestObject: public JSonSerialize<void>
	{
		public:
			TestObject():
				theStringValue("testStr", *this, "coucou"),
				theIntValue("testint", *this),
				theBoolValue("testBool", *this)
			{ }

			std::string getStr() const {return *theStringValue; }
			int getInt() const {return *theIntValue; }
			bool getBool() const { return *theBoolValue; }

			TestObject *setStr(const std::string &v)
			{ *theStringValue = v; return this; }
			TestObject *setBool(bool v)
			{ *theBoolValue = v; return this; }
			TestObject *setInt(int v)
			{ *theIntValue = v; return this; }

		protected:
			JSonSerialize<std::string> theStringValue;
			JSonSerialize<int> theIntValue;
			JSonSerialize<bool> theBoolValue;
	};

	TEST(FromString_ToString)
	{
		TestObject obj, obj2, obj3;
		std::string str;

		obj.setStr("coucou")->setInt(42)->setBool(true);
		str = obj.toString();
		CHECK_EQUAL("{testBool:true,testStr:\"coucou\",testint:42}", str);
		obj2.fromString(str);
		CHECK_EQUAL(true, obj2.getBool());
		CHECK_EQUAL(42, obj2.getInt());
		CHECK_EQUAL("coucou", obj2.getStr());
		obj3.fromString("{ testBool:true, testStr:\"coucou\", testint:42 }");
		CHECK_EQUAL(true, obj3.getBool());
		CHECK_EQUAL(42, obj3.getInt());
		CHECK_EQUAL("coucou", obj3.getStr());
		obj3.fromString("{ testBool: true, testStr: \"coucou\", testint: 42 }");
		CHECK_EQUAL(true, obj3.getBool());
		CHECK_EQUAL(42, obj3.getInt());
		CHECK_EQUAL("coucou", obj3.getStr());
		obj3.fromString("{ testBool :   true   ,  testStr   :  \"coucou\"   ,   testint :  42 } ");
		CHECK_EQUAL(true, obj3.getBool());
		CHECK_EQUAL(42, obj3.getInt());
		CHECK_EQUAL("coucou", obj3.getStr());
	}

#include "Author.hpp"

	TEST(Authors)
	{
		//std::list<Author> authors;
		//JSonSerialize<Author> a;
	}
}

